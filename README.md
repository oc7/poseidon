## Poseidon App
## Technical:

1. Framework: Spring Boot v2.0.4
2. Java 8
3. Thymeleaf
4. Bootstrap v.4.3.1

## What is Poseidon
Poseidon is a web application with API Rest which allows you to 
connect securely to your personal account, and 
which allows you to manage transactions.


## NOTES

Create a database called Poseidon and import the data.sql

Login to the app with Admin : 

Username : Admin
<br>
Password : Test123@


## DOCUMENTATION API


API documentation was generated with Swagger UI and is available at this address :

http://localhost:8080/swagger-ui.html


## Implement a Feature
1. Create mapping domain class and place in package com.nnk.springboot.domain
2. Create repository class and place in package com.nnk.springboot.repositories
3. Create controller class and place in package com.nnk.springboot.controllers
4. Create view files and place in src/main/resource/templates

## Write Unit Test
1. Create unit test and place in package com.nnk.springboot in folder test > java

## Security
1. Create user service to load user from  database and place in package com.nnk.springboot.services
2. Add configuration class and place in package com.nnk.springboot.config



