package com.nnk.poseidon.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.Collection;

import static springfox.documentation.builders.PathSelectors.any;


/**
 * Configuration Swagger UI (documentation API)
 */

@Configuration
@EnableSwagger2
public class ConfigSwagger {


    @Bean
    public Docket docket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors
                        .basePackage("com.nnk.poseidon.controllers.api"))
                .paths(any()).build().apiInfo(apiDocumentationData());
    }


    private ApiInfo apiDocumentationData() {
        Collection<VendorExtension> vendorExtensions = new ArrayList<>();
        return new ApiInfo("Spring Boot Rest Api",
                "Spring Boot Rest Api for the Poseidon Application",
                "1.0.0", "https://swagger.io/specification/",
                new Contact("Nicolas Gros",
                        "https://gitlab.com/oc7/poseidon",
                        "nicolasgros@protonmail.com"),
                "Apache 2.0",
                "https://www.apache.org/licenses/LICENSE-2.0",
                vendorExtensions);
    }
}
