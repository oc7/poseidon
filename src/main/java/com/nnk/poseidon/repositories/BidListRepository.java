package com.nnk.poseidon.repositories;

import com.nnk.poseidon.domain.BidList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BidListRepository extends JpaRepository<BidList, Integer> {
    Optional<BidList> findByAccountAndType(String account, String type);
}
