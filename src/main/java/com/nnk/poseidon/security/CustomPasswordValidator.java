package com.nnk.poseidon.security;

import com.nnk.poseidon.constants.ConstantsNumber;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.passay.MessageResolver;
import org.passay.PropertiesMessageResolver;
import org.passay.PasswordValidator;
import org.passay.LengthRule;
import org.passay.CharacterRule;
import org.passay.EnglishCharacterData;
import org.passay.WhitespaceRule;
import org.passay.RuleResult;
import org.passay.PasswordData;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.net.URL;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/**
 * Validation password with Passay policy
 *
 */

public class CustomPasswordValidator implements ConstraintValidator<ValidPassword, String> {

    private static final Logger LOGGER = LogManager.getLogger(CustomPasswordValidator.class);

    @Override
    public void initialize(final ValidPassword validPassword) {
    }

    @Override
    public boolean isValid(final String password, final ConstraintValidatorContext context) {
        LOGGER.debug("Validating user password");
        URL fileUrl = this.getClass().getClassLoader().getResource("passay.properties");
        Properties properties = new Properties();
        if (fileUrl != null) {
            try (InputStream inputStream = new FileInputStream(fileUrl.getPath());
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader)) {
                properties.load(bufferedReader);
                LOGGER.info("Passay policy success loaded");
            } catch (IOException e) {
                LOGGER.error("Passay policy failed to load");
            }
        }

        MessageResolver resolver = new PropertiesMessageResolver(properties);
        PasswordValidator validator =
                new PasswordValidator(resolver, Arrays.asList(
                        new LengthRule(ConstantsNumber.EIGHT, ConstantsNumber.ONE_HUNDRED_AND_TWENTY_FIVE),
                        new CharacterRule(EnglishCharacterData.UpperCase, 1),
                        new CharacterRule(EnglishCharacterData.LowerCase, 1),
                        new CharacterRule(EnglishCharacterData.Digit, 1),
                        new CharacterRule(EnglishCharacterData.Special, 1),
                        new WhitespaceRule()
        ));
        RuleResult result = validator.validate(new PasswordData(password));
        if (result.isValid()) {
            LOGGER.info("User password validate success");
            return true;
        }
        List<String> messages = validator.getMessages(result);
        String messageTemplate = String.join(" ", messages);
        context.buildConstraintViolationWithTemplate(messageTemplate).addConstraintViolation()
                .disableDefaultConstraintViolation();
        LOGGER.error("User password failed to validate");
        return false;
    }
}
