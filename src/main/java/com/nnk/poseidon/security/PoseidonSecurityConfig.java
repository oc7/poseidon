package com.nnk.poseidon.security;

import com.nnk.poseidon.constants.ConstantsNumber;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;


/**
 * Configuration of security, allow access and URL
 */
@Configuration
@EnableWebSecurity
public class PoseidonSecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserDetailsService userDetailsService;

    @Autowired
    public PoseidonSecurityConfig(
            @Qualifier("userDetailsServiceImpl")
            final UserDetailsService pUserDetailsService) {
        this.userDetailsService = pUserDetailsService;
    }

    @Override
    protected void configure(final AuthenticationManagerBuilder auth)
            throws Exception {
        auth.userDetailsService(userDetailsService)
                .passwordEncoder(encoder());
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http.csrf().disable().authorizeRequests()
                .antMatchers("/admin/**", "/app/**",
                        "/user/**").hasRole("ADMIN")
                .antMatchers("/bidList/**", "/curvePoint/**",
                        "/rating/**", "/ruleName/**", "/trade/**")
                .hasAnyRole("ADMIN", "USER")
                .antMatchers("/", "/api/**", "/css/**").permitAll()
                .and()
                .formLogin().loginPage("/login")
                .defaultSuccessUrl("/bidList/list")
                .failureUrl("/login").permitAll()
                .and().logout().logoutUrl("/logout").permitAll()
                .and().exceptionHandling().accessDeniedPage("/forbidden");
    }

    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder(ConstantsNumber.FIFTEEN);
    }
}
