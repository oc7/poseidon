package com.nnk.poseidon.security;

import com.nnk.poseidon.domain.User;
import com.nnk.poseidon.repositories.UserRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private static final Logger LOGGER = LogManager.getLogger(UserDetailsServiceImpl.class);
    private final UserRepository repository;

    @Autowired
    public UserDetailsServiceImpl(final UserRepository userRepository) {
        this.repository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(final String username) {
        LOGGER.debug("Loading user {}", username);
        Optional<User> user = repository.findByUsername(username);
        if (user.isPresent()) {
            LOGGER.info("User '{}' loaded successfully", username);
            return new UserDetailsImpl(user.get());
        } else {
            LOGGER.error("Failed to load user '{}'", username);
            throw new UsernameNotFoundException("Not found");
        }
    }
}
