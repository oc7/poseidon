package com.nnk.poseidon.domain;

import com.nnk.poseidon.constants.ConstantsNumber;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.NonNull;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import javax.validation.constraints.NotBlank;


@Entity
@Table(name = "rule_name")
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@RequiredArgsConstructor(access = AccessLevel.PUBLIC)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@ToString
public class RuleName {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "rule_name_id")
    private Integer id;

    @NonNull
    @NotBlank(message = "Name is mandatory")
    @Column(name = "name",
            length = ConstantsNumber.ONE_HUNDRED_AND_TWENTY_FIVE)
    private String name;

    @NonNull
    @NotBlank(message = "Description is mandatory")
    @Column(name = "description",
            length = ConstantsNumber.ONE_HUNDRED_AND_TWENTY_FIVE)
    private String description;

    @NonNull
    @NotBlank(message = "Json is mandatory")
    @Column(name = "json",
            length = ConstantsNumber.ONE_HUNDRED_AND_TWENTY_FIVE)
    private String json;

    @NonNull
    @NotBlank(message = "Template is mandatory")
    @Column(name = "template",
            length = ConstantsNumber.FIVE_HUNDREDS_AND_TWELVE)
    private String template;

    @NonNull
    @NotBlank(message = "SqlStr is mandatory")
    @Column(name = "sql_str",
            length = ConstantsNumber.ONE_HUNDRED_AND_TWENTY_FIVE)
    private String sqlStr;

    @NonNull
    @NotBlank(message = "SqlPart is mandatory")
    @Column(name = "sql_part",
            length = ConstantsNumber.ONE_HUNDRED_AND_TWENTY_FIVE)
    private String sqlPart;
}
