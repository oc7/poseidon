package com.nnk.poseidon.domain;

import com.nnk.poseidon.constants.ConstantsNumber;
import lombok.RequiredArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.NonNull;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;


@Entity
@Table(name = "trade")
@RequiredArgsConstructor(access = AccessLevel.PUBLIC)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@ToString
public class Trade {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "trade_id")
    private Integer tradeId;

    @NonNull
    @NotNull
    @NotBlank(message = "Account is mandatory")
    @Column(name = "account", length = ConstantsNumber.THIRTY)
    private String account;

    @NonNull
    @NotNull
    @NotBlank(message = "Type is mandatory")
    @Column(name = "type", length = ConstantsNumber.THIRTY)
    private String type;

    @Column(name = "buy_quantity")
    private Double buyQuantity;

    @Column(name = "sell_quantity")
    private Double sellQuantity;

    @Column(name = "buy_price")
    private Double buyPrice;

    @Column(name = "sell_price")
    private Double sellPrice;

    @Column(name = "benchmark", length = ConstantsNumber.ONE_HUNDRED_AND_TWENTY_FIVE)
    private String benchmark;

    @Column(name = "trade_date", length = ConstantsNumber.ONE_HUNDRED_AND_TWENTY_FIVE)
    private Timestamp tradeDate;

    @Column(name = "security", length = ConstantsNumber.ONE_HUNDRED_AND_TWENTY_FIVE)
    private String security;

    @Column(name = "status", length = ConstantsNumber.TEN)
    private String status;

    @Column(name = "trader", length = ConstantsNumber.ONE_HUNDRED_AND_TWENTY_FIVE)
    private String trader;

    @Column(name = "book", length = ConstantsNumber.ONE_HUNDRED_AND_TWENTY_FIVE)
    private String book;

    @Column(name = "creation_name", length = ConstantsNumber.ONE_HUNDRED_AND_TWENTY_FIVE)
    private String creationName;

    @Column(name = "creation_date")
    private Timestamp creationDate;

    @Column(name = "revision_name", length = ConstantsNumber.ONE_HUNDRED_AND_TWENTY_FIVE)
    private String revisionName;

    @Column(name = "revision_date")
    private Timestamp revisionDate;

    @Column(name = "deal_name", length = ConstantsNumber.ONE_HUNDRED_AND_TWENTY_FIVE)
    private String dealName;

    @Column(name = "deal_type", length = ConstantsNumber.ONE_HUNDRED_AND_TWENTY_FIVE)
    private String dealType;

    @Column(name = "source_list_id", length = ConstantsNumber.ONE_HUNDRED_AND_TWENTY_FIVE)
    private String sourceListId;

    @Column(name = "side", length = ConstantsNumber.ONE_HUNDRED_AND_TWENTY_FIVE)
    private String side;

    public Timestamp getTradeDate() {
        if (tradeDate == null) {
            return null;
        } else {
            return new Timestamp(tradeDate.getTime());
        }
    }

    public void setTradeDate(final Timestamp tTradeDate) {
        this.tradeDate = new Timestamp(tTradeDate.getTime());
    }

    public Timestamp getCreationDate() {
        if (creationDate == null) {
            return null;
        } else {
            return new Timestamp(creationDate.getTime());
        }
    }

    public void setCreationDate(final Timestamp tCreationDate) {
        this.creationDate = new Timestamp(tCreationDate.getTime());
    }

    public Timestamp getRevisionDate() {
        if (revisionDate == null) {
            return null;
        } else {
            return new Timestamp(revisionDate.getTime());
        }
    }

    public void setRevisionDate(final Timestamp tRevisionDate) {
        if (tRevisionDate == null) {
            this.revisionDate = null;
        } else {
            this.revisionDate = new Timestamp(tRevisionDate.getTime());
        }
    }
}
