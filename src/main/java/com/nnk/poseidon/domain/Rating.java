package com.nnk.poseidon.domain;

import com.nnk.poseidon.constants.ConstantsNumber;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.AccessLevel;
import lombok.NonNull;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@Entity
@Table(name = "rating")
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@RequiredArgsConstructor(access = AccessLevel.PUBLIC)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@ToString
public class Rating {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "rating_id")
    private Integer id;

    @NonNull
    @NotBlank(message = "Moody's Rating is mandatory")
    @Column(name = "moodys_rating",
            length = ConstantsNumber.ONE_HUNDRED_AND_TWENTY_FIVE)
    private String moodysRating;

    @NonNull
    @NotBlank(message = "S & P Rating is mandatory")
    @Column(name = "s_and_p_rating",
            length = ConstantsNumber.ONE_HUNDRED_AND_TWENTY_FIVE)
    private String sandPRating;

    @NonNull
    @NotBlank(message = "Fitch Rating is mandatory")
    @Column(name = "fitch_rating",
            length = ConstantsNumber.ONE_HUNDRED_AND_TWENTY_FIVE)
    private String fitchRating;

    @NonNull
    @NotNull
    @Column(name = "order_number")
    private Integer orderNumber;
}
