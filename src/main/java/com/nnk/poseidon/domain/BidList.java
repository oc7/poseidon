package com.nnk.poseidon.domain;

import com.nnk.poseidon.constants.ConstantsNumber;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.NonNull;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

@Entity
@Table(name = "bid_list")
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@RequiredArgsConstructor(access = AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@ToString
public class BidList {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "bid_list_id")
    private Integer bidListId;

    @NonNull
    @NotNull
    @NotBlank(message = "Account is mandatory")
    @Column(name = "account", length = ConstantsNumber.THIRTY)
    private String account;

    @NonNull
    @NotNull
    @NotBlank(message = "Type is mandatory")
    @Column(name = "type", length = ConstantsNumber.THIRTY)
    private String type;

    @NonNull
    @NotNull(message = "Bid quantity is mandatory and must be a number")
    @Column(name = "bid_quantity")
    private Double bidQuantity;

    @Column(name = "ask_quantity")
    private Double askQuantity;

    @Column(name = "bid")
    private Double bid;

    @Column(name = "ask")
    private Double ask;

    @Column(name = "benchmark",
            length = ConstantsNumber.ONE_HUNDRED_AND_TWENTY_FIVE)
    private String benchmark;

    @Column(name = "bid_list_date")
    private Timestamp bidListDate;

    @Column(name = "commentary",
            length = ConstantsNumber.ONE_HUNDRED_AND_TWENTY_FIVE)
    private String commentary;

    @Column(name = "security",
            length = ConstantsNumber.ONE_HUNDRED_AND_TWENTY_FIVE)
    private String security;

    @Column(name = "status",
            length = ConstantsNumber.TEN)
    private String status;

    @Column(name = "trader",
            length = ConstantsNumber.ONE_HUNDRED_AND_TWENTY_FIVE)
    private String trader;

    @Column(name = "book",
            length = ConstantsNumber.ONE_HUNDRED_AND_TWENTY_FIVE)
    private String book;

    @Column(name = "creation_name",
            length = ConstantsNumber.ONE_HUNDRED_AND_TWENTY_FIVE)
    private String creationName;

    @Column(name = "creation_date")
    private Timestamp creationDate;

    @Column(name = "revision_name",
            length = ConstantsNumber.ONE_HUNDRED_AND_TWENTY_FIVE)
    private String revisionName;

    @Column(name = "revision_date")
    private Timestamp revisionDate;

    @Column(name = "deal_name",
            length = ConstantsNumber.ONE_HUNDRED_AND_TWENTY_FIVE)
    private String dealName;

    @Column(name = "deal_type",
            length = ConstantsNumber.ONE_HUNDRED_AND_TWENTY_FIVE)
    private String dealType;

    @Column(name = "source_list_id",
            length = ConstantsNumber.ONE_HUNDRED_AND_TWENTY_FIVE)
    private String sourceListId;

    @Column(name = "side",
            length = ConstantsNumber.ONE_HUNDRED_AND_TWENTY_FIVE)
    private String side;

    public Timestamp getBidListDate() {
        if (bidListDate == null) {
            return null;
        } else {
            return new Timestamp(bidListDate.getTime());
        }
    }

    public void setBidListDate(final Timestamp bListDate) {
        this.bidListDate = new Timestamp(bListDate.getTime());
    }


    public Timestamp getCreationDate() {
        if (creationDate == null) {
            return null;
        } else {
            return new Timestamp(creationDate.getTime());
        }
    }

    public void setCreationDate(final Timestamp bCreationDate) {
        this.creationDate = new Timestamp(bCreationDate.getTime());
    }

    public Timestamp getRevisionDate() {
        if (revisionDate == null) {
            return null;
        } else {
            return new Timestamp(revisionDate.getTime());
        }
    }

    public void setRevisionDate(final Timestamp bRevisionDate) {
        if (bRevisionDate == null) {
            this.revisionDate = null;
        } else {
            this.revisionDate = new Timestamp(bRevisionDate.getTime());
        }
    }
}
