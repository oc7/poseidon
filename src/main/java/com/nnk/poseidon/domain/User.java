package com.nnk.poseidon.domain;

import com.nnk.poseidon.constants.ConstantsNumber;
import com.nnk.poseidon.security.ValidPassword;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.NonNull;
import lombok.NoArgsConstructor;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "user")
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@RequiredArgsConstructor(access = AccessLevel.PUBLIC)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@ToString
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Integer id;

    @NonNull
    @NotBlank(message = "Username is mandatory")
    @Column(name = "username", length = ConstantsNumber.ONE_HUNDRED_AND_TWENTY_FIVE)
    private String username;

    @NonNull
    @ValidPassword
    @NotBlank(message = "Password is mandatory and must not be black")
    @Column(name = "password", length = ConstantsNumber.ONE_HUNDRED_AND_TWENTY_FIVE)
    private String password;

    @NonNull
    @NotBlank(message = "FullName is mandatory")
    @Column(name = "full_name", length = ConstantsNumber.ONE_HUNDRED_AND_TWENTY_FIVE)
    private String fullName;

    @NonNull
    @NotBlank(message = "Role is mandatory")
    @Column(name = "role", length = ConstantsNumber.ONE_HUNDRED_AND_TWENTY_FIVE)
    private String role;
}

