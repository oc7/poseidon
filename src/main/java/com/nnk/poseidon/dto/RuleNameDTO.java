package com.nnk.poseidon.dto;

import lombok.NoArgsConstructor;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.NonNull;

import javax.validation.constraints.NotBlank;


@NoArgsConstructor(access = AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
public class RuleNameDTO {

    private Integer id;

    @NonNull
    @NotBlank(message = "Name is mandatory")
    private String name;

    @NonNull
    @NotBlank(message = "Description is mandatory")
    private String description;

    @NonNull
    @NotBlank(message = "Json is mandatory")
    private String json;

    @NonNull
    @NotBlank(message = "Template is mandatory")
    private String template;

    @NonNull
    @NotBlank(message = "SqlStr is mandatory")
    private String sqlStr;

    @NonNull
    @NotBlank(message = "SqlPart is mandatory")
    private String sqlPart;
}
