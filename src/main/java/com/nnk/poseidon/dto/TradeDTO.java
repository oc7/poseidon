package com.nnk.poseidon.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

@NoArgsConstructor
@Getter
@Setter
public class TradeDTO {

    private Integer tradeId;

    @NotNull
    @NotBlank(message = "Account is mandatory")
    private String account;

    @NotNull
    @NotBlank(message = "Type is mandatory")
    private String type;
    private Double buyQuantity;
    private Double sellQuantity;
    private Double buyPrice;
    private Double sellPrice;
    private String benchmark;
    private Timestamp tradeDate;
    private String security;
    private String status;
    private String trader;
    private String book;
    private String creationName;
    private Timestamp creationDate;
    private String revisionName;
    private Timestamp revisionDate;
    private String dealName;
    private String dealType;
    private String sourceListId;
    private String side;

    public Timestamp getTradeDate() {
        if (tradeDate == null) {
            return null;
        } else {
            return new Timestamp(tradeDate.getTime());
        }
    }

    public void setTradeDate(final Timestamp tTradeDate) {
        this.tradeDate = new Timestamp(tTradeDate.getTime());
    }

    public Timestamp getCreationDate() {
        if (creationDate == null) {
            return null;
        } else {
            return new Timestamp(creationDate.getTime());
        }
    }

    public void setCreationDate(final Timestamp tCreationDate) {
        this.creationDate = new Timestamp(tCreationDate.getTime());
    }

    public Timestamp getRevisionDate() {
        if (revisionDate == null) {
            return null;
        } else {
            return new Timestamp(revisionDate.getTime());
        }
    }

    public void setRevisionDate(final Timestamp tRevisionDate) {
        if (tRevisionDate == null) {
            this.revisionDate = null;
        } else {
            this.revisionDate = new Timestamp(tRevisionDate.getTime());
        }
    }
}
