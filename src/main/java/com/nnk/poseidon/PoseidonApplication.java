package com.nnk.poseidon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PoseidonApplication {

    public static void main(final String[] args) {
        SpringApplication.run(PoseidonApplication.class, args);
    }

    protected PoseidonApplication() {
    }
}
