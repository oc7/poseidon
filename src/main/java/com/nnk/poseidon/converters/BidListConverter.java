package com.nnk.poseidon.converters;

import com.nnk.poseidon.domain.BidList;
import com.nnk.poseidon.dto.BidListDTO;

import java.util.List;

/**
 * Conversion BidList entity to BidListDTO
 *
 */

public interface BidListConverter {

    /**
     * Converts a BidListDTO to a BidList entity.
     *
     * @param bidListDTO bidListDTO to convert
     * @return BidList entity
     */
    BidList bidListDTOToBidListEntity(BidListDTO bidListDTO);

    /**
     * Converts a list of BidListDTO to a list of BidList entities.
     *
     * @param bidListDTOList a list to convert
     * @return a list of BidList entities
     */
    List<BidList> bidListDTOsToBidListEntities(List<BidListDTO> bidListDTOList);

    /**
     * Convert BidList domain to a BidListDTO
     *
     * @param bidList bidList to convert
     * @return a bidListDTO
     */
    BidListDTO bidListEntityToBidListDTO(BidList bidList);

    /**
     * Convert list of BidList domain to list of DTO
     *
     * @param bidListList a list to convert
     * @return a list of BidListDTOs
     */
    List<BidListDTO> bidListEntitiesToBidListDTOs(List<BidList> bidListList);
}
