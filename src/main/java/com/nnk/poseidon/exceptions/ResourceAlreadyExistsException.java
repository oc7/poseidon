package com.nnk.poseidon.exceptions;

/**
 * Exception thrown when trying to save an object already existing
 */

public class ResourceAlreadyExistsException extends Exception {

    public ResourceAlreadyExistsException(final String message) {
        super(message);
    }
}
