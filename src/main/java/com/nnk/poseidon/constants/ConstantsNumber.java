package com.nnk.poseidon.constants;


public final class ConstantsNumber {

    protected ConstantsNumber() {
    }

    public static final int EIGHT = 8;
    public static final int TEN = 10;
    public static final int FIFTEEN = 15;
    public static final int THIRTY = 30;
    public static final int ONE_HUNDRED_AND_TWENTY_FIVE = 125;
    public static final int FIVE_HUNDREDS_AND_TWELVE = 512;

}
