package com.nnk.poseidon.constants;

/**
 * Constants of the app Poseidon
 */

public final class ConstantsURL {


    protected ConstantsURL() {
    }

    public static final String RULENAME_URL = "http://localhost:8080/api/ruleName";
    public static final String TRADE_URL = "http://localhost:8080/api/trade";
    public static final String USER_URL = "http://localhost:8080/api/user";
    public static final String BID_LIST_URL = "http://localhost:8080/api/bidList";
    public static final String CURVE_URL = "http://localhost:8080/api/curvePoint";
    public static final String RATING_URL = "http://localhost:8080/api/rating";

}
