package com.nnk.poseidon.controllers.api;

import com.nnk.poseidon.converters.RatingConverter;
import com.nnk.poseidon.dto.RatingDTO;
import com.nnk.poseidon.services.RatingService;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.DeleteMapping;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

/**
 * Controller API endpoints, CRUD operations on Rating
 */

@RestController
@RequestMapping("/api/rating")
public class RatingApiController {

    private static final Logger LOGGER = LogManager.getLogger(RatingApiController.class);
    private final RatingService service;
    private final RatingConverter converter;

    @Autowired
    public RatingApiController(final RatingService ratingService,
                               final RatingConverter ratingConverter) {
        this.service = ratingService;
        this.converter = ratingConverter;
    }

    /**
     * Find rating by ID
     *
     * @param id the Rating id
     * @return the Rating
     */
    @ApiOperation(value = "Find Rating by ID")
    @GetMapping("/findById/{id}")
    public Optional<RatingDTO> findRatingById(@PathVariable final Integer id) {
        LOGGER.debug("GET request findById Rating {} OK", id);
        return service.findRatingById(id);
    }

    /**
     * Find all Rating
     *
     * @return list of all Rating
     */
    @ApiOperation(value = "Find all Rating")
    @GetMapping("/findAll")
    public List<RatingDTO> findAll() {
        LOGGER.debug("GET request findAll Rating OK");
        return service.findAllRatings();
    }

    /**
     * Add/Save Rating
     *
     * @param ratingDTO the Rating to add
     * @return Rating saved success
     */
    @ApiOperation(value = "Add/Save new Rating")
    @PostMapping("/add")
    public String saveRating(@RequestBody @Valid final RatingDTO ratingDTO) {
        LOGGER.debug("POST request Add/Save Rating OK ");
        service.saveRating(converter.ratingDTOToRatingEntityConverter(ratingDTO));
        return "Rating saved success";
    }

    /**
     * Update existing Rating
     *
     * @param id        the id of Rating to update
     * @param ratingDTO the new Rating information
     * @return Rating updated success
     */
    @ApiOperation(value = "Update existing Rating")
    @PutMapping("/update/{id}")
    public String updateRating(@PathVariable final Integer id,
                               @RequestBody @Valid final RatingDTO ratingDTO) {
        LOGGER.debug("Put request updateRating OK {}", id);
        service.updateRating(id, converter.ratingDTOToRatingEntityConverter(ratingDTO));
        return "Rating updated success";
    }

    /**
     * Delete Rating by ID
     *
     * @param id the id of the Rating to delete
     * @return Rating deleted success
     */
    @ApiOperation(value = "Delete Rating by ID")
    @DeleteMapping("/delete/{id}")
    public String deleteRating(@PathVariable final Integer id) {
        LOGGER.debug("DELETE request Rating {} OK", id);
        service.deleteRating(id);
        return "Rating deleted success";
    }
}
