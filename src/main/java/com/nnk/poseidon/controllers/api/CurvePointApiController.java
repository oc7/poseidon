package com.nnk.poseidon.controllers.api;


import com.nnk.poseidon.converters.CurvePointConverter;
import com.nnk.poseidon.domain.CurvePoint;
import com.nnk.poseidon.dto.CurvePointDTO;
import com.nnk.poseidon.services.CurvePointService;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.DeleteMapping;


import javax.validation.Valid;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

/**
 * Controller API endpoints, CRUD operations on CurvePoint
 */

@RestController
@RequestMapping("/api/curvePoint")
public class CurvePointApiController {

    private static final Logger LOGGER = LogManager.getLogger(CurvePointApiController.class);


    private final CurvePointService service;
    private final CurvePointConverter converter;

    @Autowired
    public CurvePointApiController(final CurvePointService curveService, final CurvePointConverter curveConverter) {
        this.service = curveService;
        this.converter = curveConverter;
    }

    /**
     * Add CurvePoint
     *
     * @param curvePoint the CurvePointDTO to sav
     * @return Success CurvePoint saved
     */
    @ApiOperation(value = "Add CurvePoint")
    @PostMapping("/add")
    public String saveCurvePoint(@RequestBody @Valid final CurvePointDTO curvePoint) {
        LOGGER.debug("POST request save/add CurvePoint OK");
        curvePoint.setCreationDate(new Timestamp(System.currentTimeMillis()));
        CurvePoint curvePointToSave = converter.curvePointDTOToCurvePointEntity(curvePoint);
        service.saveCurvePoint(curvePointToSave);
        return "CurvePoint saved success";
    }

    /**
     * Update CurvePoint
     *
     * @param id         the CurvePoint id
     * @param curvePoint CurvePointDTO that holds the new information
     * @return CurvePoint updated successfully
     */
    @ApiOperation(value = "Update an existing CurvePoint")
    @PutMapping("/update/{id}")
    public String updateCurvePoint(@PathVariable final Integer id, @RequestBody
    @Valid final CurvePointDTO curvePoint) {
        LOGGER.debug("PUT request update CurvePoint {} OK", id);
        service.updateCurvePoint(id, converter.curvePointDTOToCurvePointEntity(curvePoint));
        return "CurvePoint updated success";
    }

    /**
     * Find all list of CurvePoint
     *
     * @return a list CurvePointDTO
     */
    @ApiOperation(value = "Find list all CurvePoint")
    @GetMapping("/findAll")
    public List<CurvePointDTO> findAll() {
        LOGGER.debug("GET request findAll CurvePoint OK");
        return service.findAllCurvePoints();
    }

    /**
     * Find CurvePoint by ID
     *
     * @param id the id of the CurvePoint
     * @return CurvePoint by the ID
     */
    @ApiOperation(value = "Find CurvePoint by ID")
    @GetMapping("/findById/{id}")
    public Optional<CurvePointDTO> findCurvePointById(
            @PathVariable final Integer id) {
        LOGGER.debug("GET request findById CurvePoint {} OK", id);
        return service.findCurvePointById(id);
    }

    /**
     * Delete CurvePoint by ID
     *
     * @param id the id of the CurvePoint to delete
     * @return CurvePoint deleted successfully
     */
    @ApiOperation(value = "Delete a CurvePoint from database by Id")
    @DeleteMapping("/delete/{id}")
    public String deleteCurvePointById(@PathVariable final Integer id) {
        LOGGER.debug("DELETE request deleteById CurvePoint {} OK", id);
        service.deleteCurvePointById(id);
        return "CurvePoint deleted success";
    }
}
