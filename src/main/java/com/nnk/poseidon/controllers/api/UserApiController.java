package com.nnk.poseidon.controllers.api;

import com.nnk.poseidon.converters.UserConverter;
import com.nnk.poseidon.domain.User;
import com.nnk.poseidon.dto.UserDTO;
import com.nnk.poseidon.exceptions.ResourceAlreadyExistsException;
import com.nnk.poseidon.services.UserService;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.DeleteMapping;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

/**
 * Controller API endpoints, CRUD operations on User
 */

@RestController
@RequestMapping("/api/user")
public class UserApiController {

    private static final Logger LOGGER = LogManager.getLogger(UserApiController.class);
    private final UserService service;
    private final UserConverter converter;
    private final PasswordEncoder encoder;

    @Autowired
    public UserApiController(final UserService userService, final UserConverter userConverter, final PasswordEncoder passwordEncoder) {
        this.service = userService;
        this.converter = userConverter;
        this.encoder = passwordEncoder;
    }

    /**
     * Find User by ID
     *
     * @param id the id of the User to find
     * @return a User
     */
    @ApiOperation(value = "Find User by ID")
    @GetMapping("/findById/{id}")
    public Optional<UserDTO> findUserById(@PathVariable final Integer id) {
        LOGGER.debug("GET request User {} OK", id);
        return service.findById(id);
    }

    /**
     * Find all User
     *
     * @return a list of all User
     */
    @ApiOperation(value = "Find all User")
    @GetMapping("/findAll")
    public List<UserDTO> findAll() {
        LOGGER.debug("GET request findAll User OK");
        return service.findAllUsers();
    }

    /**
     * @param user the User that will be saved
     * @return User saved success
     * @throws ResourceAlreadyExistsException if user already existing
     */
    @ApiOperation(value = "Create new User")
    @PostMapping("/add")
    public String saveNewUser(@Valid @RequestBody final UserDTO user) throws ResourceAlreadyExistsException {
        LOGGER.debug("POST request save User OK");
        String encodedPassword = encoder.encode(user.getPassword());
        user.setPassword(encodedPassword);
        User userToSave = converter.userDTOToUserEntityConverter(user);
        service.saveNewUser(userToSave);
        LOGGER.info("New User saved success {}", user.getId());
        return String.format("User : '%s' saved success", user.getFullName());
    }

    /**
     * Update User
     *
     * @param id   the id of the User to updated
     * @param user the new User information
     * @return User updated success
     */
    @ApiOperation(value = "Update existing User")
    @PutMapping("/update/{id}")
    public String updateUser(@PathVariable final Integer id, @Valid @RequestBody final UserDTO user) {
        LOGGER.debug("PUT request update User {} OK", id);
        String encodedPassword = encoder.encode(user.getPassword());
        user.setPassword(encodedPassword);
        User userToUpdate = converter.userDTOToUserEntityConverter(user);
        service.updateUser(id, userToUpdate);
        LOGGER.info("User {} updated success", id);
        return String.format("User %s updated success", id);
    }

    /**
     * Delete User
     *
     * @param id the id of the User to delete
     * @return User deleted success
     */
    @ApiOperation(value = "Delete User by ID")
    @DeleteMapping("/delete/{id}")
    public String deleteUser(@PathVariable final Integer id) {
        LOGGER.debug("DELETE request deleteById User {} OK", id);
        service.deleteUser(id);
        LOGGER.info("User {} deleted success", id);
        return String.format("User : %s delete success", id);
    }
}
