package com.nnk.poseidon.controllers.api;

import com.nnk.poseidon.converters.RuleNameConverter;
import com.nnk.poseidon.domain.RuleName;
import com.nnk.poseidon.dto.RuleNameDTO;
import com.nnk.poseidon.services.RuleNameService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

/**
 * Controller API endpoints, CRUD operations on RuleName
 */

@RestController
@RequestMapping("/api/ruleName")
public class RuleNameApiController {

    private static final Logger LOGGER = LogManager.getLogger(RuleNameApiController.class);
    private final RuleNameService service;
    private final RuleNameConverter converter;

    @Autowired
    public RuleNameApiController(final RuleNameService ruleNameService, final RuleNameConverter ruleNameConverter) {
        this.service = ruleNameService;
        this.converter = ruleNameConverter;
    }

    /**
     * Find RuleName by ID
     *
     * @param id RuleName id
     * @return the ruleName
     */
    @GetMapping("/findById/{id}")
    public Optional<RuleNameDTO> findById(@PathVariable final Integer id) {
        LOGGER.debug("GET request findById RuleName {} OK", id);
        return service.findRuleNameById(id);
    }

    /**
     * Find all RuleName
     *
     * @return a list of all RuleName
     */
    @GetMapping("/findAll")
    public List<RuleNameDTO> findAllRuleNames() {
        LOGGER.debug("GET request findAll RuleNames OK");
        return service.findAllRuleNames();
    }

    /**
     * Add/Save RuleName
     *
     * @param ruleName the RuleName to save
     * @return RuleName saved success
     */
    @PostMapping("/add")
    public String addRuleName(@Valid @RequestBody final RuleNameDTO ruleName) {
        LOGGER.debug("POST request add/save RuleName OK");
        RuleName ruleNameToSave = converter.ruleNamDTOToRuleNameConverter(ruleName);
        service.saveRuleName(ruleNameToSave);
        return "RuleName saved success";
    }

    /**
     * Update RuleName
     *
     * @param id          the id of the RuleName to update
     * @param ruleNameDTO the new RuleName information
     * @return RuleName Update success
     */
    @PutMapping("/update/{id}")
    public String updateRuleName(@PathVariable final Integer id, @Valid
    @RequestBody final RuleNameDTO ruleNameDTO) {
        LOGGER.debug("PUT request update RuleName {} OK", id);
        service.updateRuleName(id, converter.ruleNamDTOToRuleNameConverter(ruleNameDTO));
        return String.format("RuleName %s Update success", id);
    }

    /**
     * Delete RuleName
     *
     * @param id the id of the RuleName to delete
     * @return RuleName deleted success
     */
    @DeleteMapping("/delete/{id}")
    public String deleteRuleName(@PathVariable final Integer id) {
        LOGGER.debug("DELETE request deleteRuleName {} OK", id);
        service.deleteRuleName(id);
        return String.format("RuleName %s deleted success", id);
    }
}
