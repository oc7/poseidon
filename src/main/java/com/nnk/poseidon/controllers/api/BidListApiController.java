package com.nnk.poseidon.controllers.api;

import com.nnk.poseidon.converters.BidListConverter;
import com.nnk.poseidon.dto.BidListDTO;
import com.nnk.poseidon.services.BidListService;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.DeleteMapping;

import javax.validation.Valid;
import java.util.List;

/**
 * Controller API endpoints, CRUD operations on BidList
 */

@RestController
@RequestMapping("/api/bidList")
public class BidListApiController {


    private static final Logger LOGGER = LogManager.getLogger(BidListApiController.class);


    private final BidListService service;
    private final BidListConverter converter;


    @Autowired
    public BidListApiController(final BidListService bidListService, final BidListConverter bidListConverter) {
        this.service = bidListService;
        this.converter = bidListConverter;
    }

    /**
     * Save new BidList
     *
     * @param bidListDTO bidList dto to save
     * @return Save success
     */
    @ApiOperation(value = "Save new BidList")
    @PostMapping("/add")
    public String saveBidList(@RequestBody @Valid final BidListDTO bidListDTO) {
        LOGGER.debug("POST request save BidList OK");
        bidListDTO.setRevisionName(null);
        bidListDTO.setRevisionDate(null);
        service.save(converter.bidListDTOToBidListEntity(bidListDTO));
        return "Save BidList success";
    }

    /**
     * Update BidList
     *
     * @param id         the id
     * @param bidListDTO the bid list dto
     * @return the string
     */
    @ApiOperation(value = "Update BidList")
    @PutMapping("/update/{id}")
    public String updateBidList(@PathVariable final Integer id, @RequestBody
    @Valid final BidListDTO bidListDTO) {
        LOGGER.debug("PUT request update BidList OK {}", id);
        service.updateBidList(id, converter.bidListDTOToBidListEntity(bidListDTO));
        return "Update BidList success";
    }

    /**
     * Find All BidList
     *
     * @return a list of BidListDTOs
     */
    @ApiOperation(value = "Find all BidLists")
    @GetMapping("/findAll")
    public List<BidListDTO> findAllBidLists() {
        LOGGER.debug("GET request findAll BidLists OK");
        return service.findAll();
    }

    /**
     * Delete BidList by ID
     *
     * @param id id of the BidList to delete
     * @return a message that indicates a successful operation
     */
    @ApiOperation(value = "Delete BidList by Id")
    @DeleteMapping("/delete/{id}")
    public String deleteBidListById(@PathVariable final Integer id) {
        LOGGER.debug("DELETE request deleteById BidList OK");
        service.deleteById(id);
        return "BidList deleted successfully";
    }

    /**
     * Find BidList by id
     *
     * @param id the id
     * @return the bid list dto
     */
    @ApiOperation(value = "Find BidList by Id")
    @GetMapping("/findById/{id}")
    public BidListDTO findBidListById(@PathVariable final Integer id) {
        LOGGER.debug("GET request  findBidListById {} OK", id);
        return service.findBidListById(id);
    }




}
