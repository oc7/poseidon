package com.nnk.poseidon.controllers.api;

import com.nnk.poseidon.converters.TradeConverter;
import com.nnk.poseidon.domain.Trade;
import com.nnk.poseidon.dto.TradeDTO;
import com.nnk.poseidon.services.TradeService;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;

import javax.validation.Valid;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

/**
 * Controller API endpoints, CRUD operations on Trade
 */
@RestController
@RequestMapping("/api/trade")
public class TradeApiController {

    private static final Logger LOGGER = LogManager.getLogger(TradeApiController.class);
    private final TradeService service;
    private final TradeConverter converter;

    @Autowired
    public TradeApiController(final TradeService tradeService, final TradeConverter tradeConverter) {
        this.service = tradeService;
        this.converter = tradeConverter;
    }

    /**
     * Find Trade by ID
     *
     * @param id the id of the Trade
     * @return the Trade
     */
    @ApiOperation(value = "Find Trade by ID")
    @GetMapping("/findById/{id}")
    public Optional<TradeDTO> findById(@PathVariable final Integer id) {
        LOGGER.debug("GET request findById Trade {} OK", id);
        return service.findTradeById(id);
    }

    /**
     * Find all Trade
     *
     * @return a list of all Trades
     */
    @ApiOperation(value = "Find All Trade")
    @GetMapping("/findAll")
    public List<TradeDTO> findAll() {
        LOGGER.debug("GET request Trade OK");
        return service.findAllTrades();
    }

    /**
     * Add/Save Trade
     *
     * @param trade the Trade to save
     * @return Trade saved success
     */
    @ApiOperation(value = "Add/Save new Trade")
    @PostMapping("/add")
    public String saveTrade(@Valid @RequestBody final TradeDTO trade) {
        LOGGER.debug("POST request Trade {} OK", trade.getTradeId());
        trade.setCreationDate(new Timestamp(System.currentTimeMillis()));
        trade.setRevisionName(null);
        trade.setRevisionDate(null);
        Trade tradeToSave = converter.tradeDTOToTradeEntityConverter(trade);
        service.saveTrade(tradeToSave);
        return "Trade saved success";
    }

    /**
     * Update Trade
     *
     * @param id       the id of the Trade to update
     * @param tradeDTO the new Trade information
     * @return Trade Update success
     */
    @ApiOperation(value = "Update an existing Trade")
    @PutMapping("/update/{id}")
    public String updateTrade(@PathVariable final Integer id, @Valid @RequestBody final TradeDTO tradeDTO) {
        LOGGER.debug("PUT request Trade {} OK", id);
        tradeDTO.setRevisionDate(new Timestamp(System.currentTimeMillis()));
        Trade tradeToUpdate = converter.tradeDTOToTradeEntityConverter(tradeDTO);
        service.updateTrade(id, tradeToUpdate);
        return String.format("Trade %s Updated success", id);
    }

    /**
     * Delete Trade by ID
     *
     * @param id the id of the Trade to delete
     * @return Trade delete success
     */
    @ApiOperation(value = "Delete Trade by ID")
    @DeleteMapping("/delete/{id}")
    public String deleteTrade(@PathVariable final Integer id) {
        LOGGER.debug("DELETE request Trade {} OK", id);
        service.deleteTrade(id);
        return String.format("Trade %s deleted Success", id);
    }
}
