package com.nnk.poseidon.controllers.web;

import com.nnk.poseidon.constants.ConstantsURL;
import com.nnk.poseidon.dto.BidListDTO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;
import java.sql.Timestamp;
import java.util.List;

/**
 * BidList controller allow CRUD operations on BidList
 */

@Controller
@RequestMapping("/bidList")
public class BidListController {

    private static final Logger LOGGER = LogManager.getLogger(BidListController.class);
    private static final String BID_LIST_ATTRIBUTE = "bidList";
    private static final String REDIRECTION_TO_BID_LIST_LIST = "redirect:/bidList/list";
    private static final String BID_LIST_LIST = "bidListList";
    private final RestTemplate template;

    @Autowired
    public BidListController(final RestTemplate restTemplate) {
        this.template = restTemplate;
    }

    /**
     * List of all BidList
     *
     * @param model the model
     * @return HTML list BidList
     */
    @GetMapping("/list")
    public String home(final Model model) {
        LOGGER.debug("GET request sent from the bidListController"
                + " to load all the BidLists");
        String findAllBidListsUrl = ConstantsURL.BID_LIST_URL + "/findAll";
        ResponseEntity<List<BidListDTO>> responseEntity = template.exchange(
                findAllBidListsUrl,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<BidListDTO>>() {
                }
        );
        model.addAttribute(BID_LIST_LIST, responseEntity.getBody());
        return "bidList/list";
    }

    /**
     * Add bid form string.
     *
     * @param model the model
     * @return HTML add BidList
     */
    @GetMapping("/add")
    public String addBidForm(final Model model) {
        LOGGER.debug("GET request add/save BidList OK");
        BidListDTO bidListToSave = new BidListDTO();
        model.addAttribute(BID_LIST_ATTRIBUTE, bidListToSave);
        return "bidList/add";
    }

    /**
     * Validate BidList
     *
     * @param bidListDTO the bidList to save
     * @param result     the result
     * @param model      the model
     * @return the string
     */
    @PostMapping("/validate")
    public String validate(@Valid @ModelAttribute("bidList") final BidListDTO bidListDTO,
                           final BindingResult result,
                           final Model model) {
        LOGGER.debug("POST request validate/save BidList OK");
        String addBidListUrl = ConstantsURL.BID_LIST_URL + "/add";
        if (!result.hasFieldErrors()) {
            bidListDTO.setBidListDate(new Timestamp(System.currentTimeMillis()));
            bidListDTO.setCreationDate(new Timestamp(System.currentTimeMillis()));
            bidListDTO.setRevisionName(null);
            bidListDTO.setRevisionDate(null);
            HttpEntity<BidListDTO> httpEntity = new HttpEntity<>(bidListDTO);
            template.exchange(addBidListUrl, HttpMethod.POST, httpEntity, String.class);
            return REDIRECTION_TO_BID_LIST_LIST;
        }
        LOGGER.error("BidList validate failed");
        model.addAttribute(BID_LIST_ATTRIBUTE, bidListDTO);
        return "bidList/add";
    }

    /**
     * Update BidList
     *
     * @param id    the id
     * @param model the model
     * @return the string
     */
    @GetMapping("/update")
    public String showUpdateForm(@RequestParam("id") final Integer id, final Model model) {
        LOGGER.debug("GET request update BidList OK");
        try {
            String findBidIdUrl = ConstantsURL.BID_LIST_URL + "/findById/" + id;
            ResponseEntity<BidListDTO> responseEntity = template.exchange(
                    findBidIdUrl, HttpMethod.GET, null, BidListDTO.class);
            model.addAttribute(BID_LIST_ATTRIBUTE, responseEntity.getBody());
            LOGGER.info("Update BidList {} OK", id);
        } catch (HttpStatusCodeException e) {
            LOGGER.error("Failed update BidList {} " + "BidList not found", id);
            return "404NotFound/404";
        }
        return "bidList/update";
    }

    /**
     * Update BidList by ID
     *
     * @param id      the id
     * @param bidList the bid list
     * @param result  the result
     * @param model   the model
     * @return the string
     */
    @PostMapping("/update/{id}")
    public String updateBid(@PathVariable("id") final Integer id, @Valid @ModelAttribute("bidList") final BidListDTO bidList,
                            final BindingResult result, final Model model) {
        LOGGER.debug("POST request update BidList {} OK", id);
        String updateBidListUrl = ConstantsURL.BID_LIST_URL + "/update/" + id;
        if (!result.hasFieldErrors()) {
            bidList.setRevisionDate(new Timestamp(System.currentTimeMillis()));
            HttpEntity<BidListDTO> httpEntity = new HttpEntity<>(bidList);
            template.exchange(updateBidListUrl, HttpMethod.PUT, httpEntity, String.class);
            return REDIRECTION_TO_BID_LIST_LIST;
        }
        LOGGER.error("Failed to validate BidList {}. Update form reloaded",
                bidList.getBidListId());
        model.addAttribute(BID_LIST_ATTRIBUTE, bidList);
        return "bidList/update";
    }

    /**
     * Delete BidList
     *
     * @param id the id
     * @return the string
     */
    @GetMapping("/delete")
    public String deleteBid(@RequestParam("id") final Integer id) {
        LOGGER.debug("DELETE request BidList {} OK", id);
        try {
            String deleteBidListUrl = ConstantsURL.BID_LIST_URL + "/delete/" + id;
            template.exchange(deleteBidListUrl, HttpMethod.DELETE, null, String.class);
            LOGGER.info("BidList {} deleted success", id);
        } catch (HttpServerErrorException e) {
            LOGGER.error("Delete BidList failed {}", id);
            return "404NotFound/404";
        }
        return REDIRECTION_TO_BID_LIST_LIST;
    }
}
