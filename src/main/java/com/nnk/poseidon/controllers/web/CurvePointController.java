package com.nnk.poseidon.controllers.web;

import com.nnk.poseidon.constants.ConstantsURL;
import com.nnk.poseidon.dto.CurvePointDTO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;
import java.sql.Timestamp;
import java.util.List;

/**
 * CurvePoint controller allow CRUD operations on CurvePoint
 */

@Controller
@RequestMapping("/curvePoint")
public class CurvePointController {

    private static final Logger LOGGER = LogManager.getLogger(CurvePointController.class);
    private static final String CURVE_POINT_ATTRIBUTE = "curvePoint";
    private static final String REDIRECTION_URL = "redirect:/curvePoint/list";
    private final RestTemplate template;

    @Autowired
    public CurvePointController(final RestTemplate restTemplate) {
        this.template = restTemplate;
    }

    /**
     * Home CurvePoint list
     *
     * @param model the model
     * @return HTML with all existing CurvePoint
     */
    @GetMapping("/list")
    public String home(final Model model) {
        LOGGER.debug("GET request listAll CurvePoint OK");
        String findAllCurvePointUrl = ConstantsURL.CURVE_URL + "/findAll";
        ResponseEntity<List<CurvePointDTO>> responseEntity = template.exchange(
                findAllCurvePointUrl, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<CurvePointDTO>>() {
                }
        );
        model.addAttribute("curvePointList", responseEntity.getBody());
        return "curvePoint/list";
    }

    /**
     * Add/Save CurvePoint
     *
     * @param model the model
     * @return HTML for Add/Save CurvePoint
     */
    @GetMapping("/add")
    public String addCurvePointForm(final Model model) {
        LOGGER.debug("GET request sent add/save CurvePoint OK");
        CurvePointDTO curvePointToSave = new CurvePointDTO();
        model.addAttribute(CURVE_POINT_ATTRIBUTE, curvePointToSave);
        return "curvePoint/add";
    }

    /**
     * Validate CurvePoint to save
     *
     * @param curvePoint the CurvePoint to save
     * @param result     the result
     * @param model      the model
     * @return the CurvePoint saved
     */
    @PostMapping("/validate")
    public String validate(@Valid
                           @ModelAttribute("curvePoint") final CurvePointDTO curvePoint,
                           final BindingResult result, final Model model) {
        LOGGER.debug("POST request validate CurvePoint OK");
        String addCurvePointUrl = ConstantsURL.CURVE_URL + "/add";
        if (!result.hasErrors()) {
            curvePoint.setCreationDate(new Timestamp(System.currentTimeMillis()));
            HttpEntity<CurvePointDTO> httpEntity = new HttpEntity<>(curvePoint);
            template.exchange(addCurvePointUrl, HttpMethod.POST, httpEntity, String.class);
            LOGGER.info("CurvePoint {} saved success", curvePoint.getCurveId());
            return REDIRECTION_URL;
        }
        LOGGER.error("CurvePoint save failed");
        model.addAttribute(CURVE_POINT_ATTRIBUTE, curvePoint);
        return "curvePoint/add";
    }

    /**
     * Update CurvePoint
     *
     * @param id    id of the CurvePoint to update
     * @param model the model
     * @return the HTML CurvePoint updated
     */
    @GetMapping("/update")
    public String showUpdateForm(@RequestParam("id") final Integer id, final Model model) {
        LOGGER.debug("GET request update CurvePoint {} OK", id);
        try {
            String findCurveByIdUrl = ConstantsURL.CURVE_URL + "/findById/" + id;
            ResponseEntity<CurvePointDTO> responseEntity = template.exchange(
                    findCurveByIdUrl, HttpMethod.GET, null, CurvePointDTO.class);
            model.addAttribute(CURVE_POINT_ATTRIBUTE, responseEntity.getBody());
            LOGGER.info("Update CurvePoint success {}", id);
        } catch (HttpServerErrorException e) {
            LOGGER.error("Load CurvePoint failed {}", id);
            return "404NotFound/404";
        }
        LOGGER.info("CurvePoint update success {}", id);
        return "curvePoint/update";
    }

    /**
     * Update CurvePoint by ID
     *
     * @param id         id of the CurvePoint
     * @param curvePoint the CurvePoint to update
     * @param result     the result
     * @param model      the model
     * @return HTML CurvePoint updated
     */
    @PostMapping("/update/{id}")
    public String updateCurvePoint(@PathVariable("id") final Integer id,
                                   @Valid @ModelAttribute("curvePoint") final CurvePointDTO curvePoint,
                                   final BindingResult result, final Model model) {
        LOGGER.debug("POST request update CurvePoint {} OK", id);
        String updateCurvePoint = ConstantsURL.CURVE_URL + "/update/" + id;
        if (!result.hasErrors()) {
            HttpEntity<CurvePointDTO> httpEntity = new HttpEntity<>(curvePoint);
            template.exchange(updateCurvePoint, HttpMethod.PUT, httpEntity, String.class);
            LOGGER.info("CurvePoint updated success {}", id);
            return REDIRECTION_URL;
        }
        LOGGER.error("CurvePoint update failed {}", id);
        model.addAttribute(CURVE_POINT_ATTRIBUTE, curvePoint);
        return "curvePoint/update";
    }

    /**
     * Delete CurvePoint by ID
     *
     * @param id the id CurvePoint to delete
     * @return HTML listAll CurvePoint
     */
    @GetMapping("/delete")
    public String deleteCurve(@RequestParam("id") final Integer id) {
        LOGGER.debug("GET request deleteById CurvePoint {} OK", id);
        try {
            String deleteCurvePoint = ConstantsURL.CURVE_URL + "/delete/" + id;
            template.exchange(deleteCurvePoint, HttpMethod.DELETE, null, String.class);
            LOGGER.info("CurvePoint delete success {}", id);
        } catch (HttpServerErrorException e) {
            LOGGER.error("No CurvePoint to delete with ID : {} ", id);
            return "404NotFound/404";
        }
        return REDIRECTION_URL;
    }
}
