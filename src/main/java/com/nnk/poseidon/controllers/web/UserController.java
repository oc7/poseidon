package com.nnk.poseidon.controllers.web;

import com.nnk.poseidon.constants.ConstantsURL;
import com.nnk.poseidon.dto.UserDTO;
import com.nnk.poseidon.exceptions.ResourceAlreadyExistsException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;
import java.util.List;

/**
 * User controller allow CRUD operations on User
 */

@Controller
@RequestMapping("/user")
public class UserController {

    private static final Logger LOGGER = LogManager.getLogger(UserController.class);
    private static final String USER_LIST = "users";
    private static final String REDIRECTION_LINK = "redirect:/user/list";
    private final RestTemplate template;

    public UserController(final RestTemplate restTemplate) {
        this.template = restTemplate;
    }

    /**
     * Home page list of all users
     *
     * @param model the model
     * @return HTML page with all users
     */
    @GetMapping("/list")
    public String home(final Model model) {
        LOGGER.debug("GET request listAll User OK");
        String findAllUsersUrl = ConstantsURL.USER_URL + "/findAll";
        ResponseEntity<List<UserDTO>> responseEntity = template.exchange(
                findAllUsersUrl, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<UserDTO>>() {
                }
        );
        model.addAttribute(USER_LIST, responseEntity.getBody());
        return "user/list";
    }

    /**
     * Add/Save User
     *
     * @param model the model
     * @return HTML form for adding user
     */
    @GetMapping("/add")
    public String addUser(final Model model) {
        LOGGER.debug("GET request add new User OK");
        UserDTO user = new UserDTO();
        model.addAttribute("user", user);
        return "user/add";
    }

    /**
     * Validate save new User
     *
     * @param user   the User to save
     * @param result the result
     * @param model  the model
     * @return HTML redirect to home page user or error form
     * @throws ResourceAlreadyExistsException when user already exist
     */
    @PostMapping("/validate")
    public String validate(@Valid @ModelAttribute("user") final UserDTO user,
                           final BindingResult result, final Model model)
            throws ResourceAlreadyExistsException {
        LOGGER.debug("POST request validate new User OK");
        String addUserUrl = ConstantsURL.USER_URL + "/add";
        if (!result.hasErrors()) {
            HttpEntity<UserDTO> httpEntity = new HttpEntity<>(user);
            template.exchange(addUserUrl, HttpMethod.POST, httpEntity, String.class);
            LOGGER.info("User validate success");
            return REDIRECTION_LINK;
        } else {
            LOGGER.error("User validate failed");
            model.addAttribute("user", user);
            return "user/add";
        }
    }

    /**
     * Update User
     *
     * @param id    the id of the User to updated
     * @param model the model
     * @return HTML form for update User or error form
     */
    @GetMapping("/update")
    public String showUpdateForm(@RequestParam final Integer id, final Model model) {
        LOGGER.debug("GET request update User {} OK", id);
        String findUserByIdUrl = ConstantsURL.USER_URL + "/findById/" + id;
        try {
            ResponseEntity<UserDTO> responseEntity = template.exchange(
                    findUserByIdUrl, HttpMethod.GET, null, UserDTO.class);
            model.addAttribute("user", responseEntity.getBody());
            LOGGER.info("User update success");
        } catch (HttpServerErrorException e) {
            LOGGER.error("User update failed with ID : {}", id);
            return "404NotFound/404";
        }
        return "user/update";
    }

    /**
     * Update user by ID
     *
     * @param id     the id of the User to update
     * @param user   the new User information
     * @param result the result
     * @param model  the model
     * @return HTML redirect to home page list of User or error form
     */
    @PostMapping("/update/{id}")
    public String updateUser(@PathVariable("id") final Integer id,
                             @Valid @ModelAttribute("user") final UserDTO user,
                             final BindingResult result, final Model model) {
        LOGGER.debug("POST request updateById User {} OK", id);
        String updateUserUrl = ConstantsURL.USER_URL + "/update/" + id;
        if (!result.hasErrors()) {
            HttpEntity<UserDTO> httpEntity = new HttpEntity<>(user);
            template.exchange(updateUserUrl, HttpMethod.PUT, httpEntity, String.class);
            LOGGER.info("User update success {}", id);
            return REDIRECTION_LINK;
        }
        LOGGER.error("User update failed by ID : {} ", id);
        model.addAttribute("user", user);
        return "user/update";
    }

    /**
     * Delete User
     *
     * @param id the id of the User to delete
     * @return HTML redirect to home page user or error delete user by ID not found
     */
    @GetMapping("/delete")
    public String deleteUser(@RequestParam final Integer id) {
        LOGGER.debug("GET request delete User {} OK", id);
        String deleteUserUrl = ConstantsURL.USER_URL + "/delete/" + id;
        try {
            template.exchange(deleteUserUrl, HttpMethod.DELETE, null, String.class);
            LOGGER.info("User delete success {}", id);
        } catch (HttpServerErrorException e) {
            LOGGER.error("User delete failed with ID : {}", id);
            return "404NotFound/404";
        }
        return REDIRECTION_LINK;
    }
}
