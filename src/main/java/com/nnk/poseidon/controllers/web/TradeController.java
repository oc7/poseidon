package com.nnk.poseidon.controllers.web;

import com.nnk.poseidon.constants.ConstantsURL;
import com.nnk.poseidon.dto.TradeDTO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;
import java.sql.Timestamp;
import java.util.List;

/**
 * Trade controller allow CRUD operations on Trade
 */

@Controller
@RequestMapping("/trade")
public class TradeController {

    private static final Logger LOGGER = LogManager.getLogger(TradeController.class);
    private static final String TRADE_ATTRIBUTE = "trade";
    private static final String TRADE_LIST_ATTRIBUTE = "tradeList";
    private static final String REDIRECTION_LINK = "redirect:/trade/list";
    private final RestTemplate template;

    @Autowired
    public TradeController(final RestTemplate restTemplate) {
        this.template = restTemplate;
    }

    /**
     * Home page list of all Trade
     *
     * @param model the model
     * @return HTML home page of Trade list
     */
    @GetMapping("/list")
    public String home(final Model model) {
        LOGGER.debug("GET request listAll Trade OK");
        String findAllUrl = ConstantsURL.TRADE_URL + "/findAll";
        ResponseEntity<List<TradeDTO>> responseEntity = template.exchange(
                findAllUrl, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<TradeDTO>>() {
                }
        );
        model.addAttribute(TRADE_LIST_ATTRIBUTE, responseEntity.getBody());
        return "trade/list";
    }

    /**
     * Add/Save Trade
     *
     * @param model the model
     * @return HTML form for Save/Add new Trade
     */
    @GetMapping("/add")
    public String addTrade(final Model model) {
        LOGGER.debug("GET request Add/Save Trade OK");
        TradeDTO trade = new TradeDTO();
        model.addAttribute(TRADE_ATTRIBUTE, trade);
        return "trade/add";
    }

    /**
     * Validate Trade
     *
     * @param trade  the Trade to save
     * @param result the result
     * @param model  the model
     * @return HTML redirect home page Trade or form error
     */
    @PostMapping("/validate")
    public String validate(@Valid @ModelAttribute("trade") final TradeDTO trade,
                           final BindingResult result, final Model model) {
        LOGGER.debug("POST request validate Trade OK");
        String addTradeUrl = ConstantsURL.TRADE_URL + "/add";
        if (!result.hasErrors()) {
            trade.setTradeDate(new Timestamp(System.currentTimeMillis()));
            trade.setCreationDate(new Timestamp(System.currentTimeMillis()));
            trade.setRevisionName(null);
            trade.setRevisionDate(null);
            HttpEntity<TradeDTO> httpEntity = new HttpEntity<>(trade);
            template.exchange(addTradeUrl, HttpMethod.POST, httpEntity, String.class);
            LOGGER.info("Trade validate success");
            return REDIRECTION_LINK;
        } else {
            LOGGER.error("Trade validate error, please retry");
            model.addAttribute(TRADE_ATTRIBUTE, trade);
            return "trade/add";
        }
    }

    /**
     * Update Trade
     *
     * @param id    the id of the Trade to update
     * @param model the model
     * @return HTML form for update Trade or error form
     */
    @GetMapping("/update")
    public String showUpdateForm(@RequestParam final Integer id, final Model model) {
        LOGGER.debug("GET request update Trade {} OK", id);
        try {
            String findTradeById = ConstantsURL.TRADE_URL + "/findById/" + id;
            ResponseEntity<TradeDTO> responseEntity = template.exchange(
                    findTradeById, HttpMethod.GET, null, TradeDTO.class);
            model.addAttribute(TRADE_ATTRIBUTE, responseEntity.getBody());
            LOGGER.info("Update Trade success {}", id);
        } catch (HttpServerErrorException e) {
            LOGGER.error("Update Trade failed with ID : {}", id);
            return "404NotFound/404";
        }
        return "trade/update";
    }

    /**
     * Update Trade by ID
     *
     * @param id     the id of the Trade to update
     * @param trade  the new Trade information
     * @param result the result
     * @param model  the model
     * @return HTML redirect to home page Trade or error form
     */
    @PostMapping("/update/{id}")
    public String updateTrade(@PathVariable("id") final Integer id,
                              @Valid @ModelAttribute("trade") final TradeDTO trade,
                              final BindingResult result, final Model model) {
        LOGGER.debug("POST request updateByID Trade OK {} ", id);
        String updateTradeUrl = ConstantsURL.TRADE_URL + "/update/" + id;
        if (!result.hasErrors()) {
            trade.setRevisionDate(new Timestamp(System.currentTimeMillis()));
            HttpEntity<TradeDTO> httpEntity = new HttpEntity<>(trade);
            template.exchange(updateTradeUrl, HttpMethod.PUT, httpEntity, String.class);
            LOGGER.info("Trade update by ID success {} ", id);
            return REDIRECTION_LINK;
        }
        model.addAttribute(TRADE_ATTRIBUTE, trade);
        LOGGER.error("Trade update failed with ID : {} ", id);
        return "trade/update";
    }

    /**
     * Delete Trade
     *
     * @param id the id of the Trade to delete
     * @return HTML redirect to home page Trade or error form
     */
    @GetMapping("/delete")
    public String deleteTrade(@RequestParam final Integer id) {
        LOGGER.debug("GET request delete Trade {} OK", id);
        String deleteTradeUrl = ConstantsURL.TRADE_URL + "/delete/" + id;
        try {
            template.exchange(deleteTradeUrl, HttpMethod.DELETE, null, String.class);
            LOGGER.info("Trade delete success {}", id);
        } catch (HttpServerErrorException e) {
            LOGGER.error("Trade delete failed with ID : {}", id);
            return "404NotFound/404";
        }
        return REDIRECTION_LINK;
    }
}
