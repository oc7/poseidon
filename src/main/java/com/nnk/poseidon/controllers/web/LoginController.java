package com.nnk.poseidon.controllers.web;

import com.nnk.poseidon.services.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 *  Allow user access to the application
 *
 */

@Controller
public class LoginController {

    private static final Logger LOGGER = LogManager.getLogger(LoginController.class);
    private final UserService service;

    @Autowired
    public LoginController(final UserService userService) {
        this.service = userService;
    }

    /**
     * Access to the login home page
     *
     * @return HTML form login
     */
    @GetMapping("/login")
    public String login() {
        LOGGER.debug("GET request login home page OK");
        return "login";
    }

    /**
     * List of all users
     *
     * @return HTML list of users
     */
    @GetMapping("/app/secure/article-details")
    public ModelAndView getAllUserArticles() {
        LOGGER.debug("GET request list details of all users OK");
        ModelAndView view = new ModelAndView();
        view.addObject("users", service.findAllUsers());
        view.setViewName("user/list");
        return view;
    }

    /**
     * Access denied for user unauthorized
     *
     * @return HTML page forbidden access 403
     */
    @GetMapping("/forbidden")
    public String error() {
        LOGGER.debug("Unauthorized user");
        return "403";
    }
}
