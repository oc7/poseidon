package com.nnk.poseidon.controllers.web;

import com.nnk.poseidon.constants.ConstantsURL;
import com.nnk.poseidon.dto.RuleNameDTO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;
import java.util.List;

/**
 * RuleName controller allow CRUD operations on RuleName
 */

@Controller
@RequestMapping("/ruleName")
public class RuleNameController {

    private static final Logger LOGGER = LogManager.getLogger(RuleNameController.class);
    private static final String RULE_NAME_ATTRIBUTE = "ruleName";
    private static final String REDIRECTION_LINK = "redirect:/ruleName/list";
    private final RestTemplate template;


    @Autowired
    public RuleNameController(final RestTemplate restTemplate) {
        this.template = restTemplate;
    }

    /**
     * Home page list of all RuleName
     *
     * @param model the model
     * @return HTML home page RuleName
     */
    @GetMapping("/list")
    public String home(final Model model) {
        LOGGER.debug("GET request home page of listAll RuleName OK");
        String findAllRuleNamesUrl = ConstantsURL.RULENAME_URL + "/findAll";
        ResponseEntity<List<RuleNameDTO>> responseEntity = template.exchange(
                findAllRuleNamesUrl, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<RuleNameDTO>>() {
                }
        );
        model.addAttribute("ruleNameList", responseEntity.getBody());
        return "ruleName/list";
    }

    /**
     * Add/save RuleName
     *
     * @param model the model
     * @return HTML redirect to form add/save RuleName
     */
    @GetMapping("/add")
    public String addRuleForm(final Model model) {
        LOGGER.debug("GET request Add/Save new RuleName OK");
        RuleNameDTO ruleName = new RuleNameDTO();
        model.addAttribute(RULE_NAME_ATTRIBUTE, ruleName);
        return "ruleName/add";
    }

    /**
     * Validate RuleName after adding RuleName
     *
     * @param ruleNameDTO the RuleName to save
     * @param result      the result
     * @param model       the model
     * @return HTML redirect to home page RuleName or form error
     */
    @PostMapping("/validate")
    public String validate(@Valid @ModelAttribute("ruleName") final RuleNameDTO ruleNameDTO,
                           final BindingResult result, final Model model) {
        LOGGER.debug("POST request validate RuleName OK");
        String addRuleName = ConstantsURL.RULENAME_URL + "/add";
        if (!result.hasErrors()) {
            HttpEntity<RuleNameDTO> httpEntity = new HttpEntity<>(ruleNameDTO);
            template.exchange(addRuleName, HttpMethod.POST, httpEntity, String.class);
            LOGGER.info("RuleName saved success");
            return REDIRECTION_LINK;
        }
        LOGGER.error("RuleName saved failed");
        model.addAttribute(RULE_NAME_ATTRIBUTE, ruleNameDTO);
        return "ruleName/add";
    }

    /**
     * Update RuleName
     *
     * @param id    the id of the RuleName to update
     * @param model the model
     * @return HTML form if RuleName ID is correct or error form
     */
    @GetMapping("/update")
    public String showUpdateForm(@RequestParam final Integer id, final Model model) {
        LOGGER.debug("GET request update RuleName OK");
        try {
            String findRuleNameById = ConstantsURL.RULENAME_URL + "/findById/" + id;
            ResponseEntity<RuleNameDTO> responseEntity = template.exchange(
                    findRuleNameById, HttpMethod.GET, null, RuleNameDTO.class);
            model.addAttribute(RULE_NAME_ATTRIBUTE, responseEntity.getBody());
            LOGGER.info("RuleName update success {}", id);
        } catch (HttpServerErrorException e) {
            LOGGER.error("RuleName update failed with ID : {}", id);
            return "404NotFound/404";
        }
        return "ruleName/update";
    }

    /**
     * Update RuleName by ID
     *
     * @param id          the id of the RuleName to update
     * @param ruleNameDTO the new RuleName to update
     * @param result      the result
     * @param model       the model
     * @return HTML redirect to home page RuleName or form error
     */
    @PostMapping("/update/{id}")
    public String updateRuleName(@PathVariable("id") final Integer id,
                                 @Valid @ModelAttribute("ruleName") final RuleNameDTO ruleNameDTO,
                                 final BindingResult result, final Model model) {
        LOGGER.debug("POST request updateById RuleName {} OK", id);
        String updateRuleName = ConstantsURL.RULENAME_URL + "/update/" + id;
        if (!result.hasErrors()) {
            HttpEntity<RuleNameDTO> httpEntity = new HttpEntity<>(ruleNameDTO);
            template.exchange(updateRuleName, HttpMethod.PUT, httpEntity, String.class);
            LOGGER.info("RuleName update success {} ", id);
            return REDIRECTION_LINK;
        }
        LOGGER.error("RuleName update failed with ID : {}", id);
        model.addAttribute(RULE_NAME_ATTRIBUTE, ruleNameDTO);
        return "ruleName/update";
    }

    /**
     * Delete RuleName
     *
     * @param id of the RuleName to delete
     * @return HTML redirect to home page RuleName or form error
     */
    @GetMapping("/delete")
    public String deleteRuleName(@RequestParam("id") final Integer id) {
        LOGGER.debug("GET request delete RuleName {} OK", id);
        try {
            String deleteUrl = ConstantsURL.RULENAME_URL + "/delete/" + id;
            template.exchange(deleteUrl, HttpMethod.DELETE, null, String.class);
            LOGGER.info("RuleName delete success {}", id);
        } catch (HttpServerErrorException e) {
            LOGGER.error("RuleName delete failed with ID : {} ", id);
            return "404NotFound/404";
        }
        return REDIRECTION_LINK;
    }
}
