package com.nnk.poseidon.controllers.web;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * HomeController, the home page of the application
 */

@Controller
public class HomeController {


    private static final Logger LOGGER = LogManager.getLogger(HomeController.class);

    /**
     * Home Page of the application
     *
     * @return HTML home page
     */
    @GetMapping("/")
    public String home() {
        LOGGER.debug("GET request home application OK");
        return "home";
    }

    /**
     * Admin home page
     *
     * @return BidList home page
     */
    @GetMapping("/admin/home")
    public String adminHome() {
        LOGGER.debug("GET request admin home page OK");
        return "redirect:/bidList/list";
    }
}
