package com.nnk.poseidon.controllers.web;

import com.nnk.poseidon.constants.ConstantsURL;
import com.nnk.poseidon.dto.RatingDTO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;
import java.util.List;

/**
 * Rating controller allow CRUD operations on Rating
 */

@Controller
@RequestMapping("/rating")
public class RatingController {

    private static final Logger LOGGER = LogManager.getLogger(RatingController.class);
    private static final String RATING_ATTRIBUTE = "rating";
    private static final String REDIRECTION_LINK = "redirect:/rating/list";
    private final RestTemplate template;

    @Autowired
    public RatingController(final RestTemplate restTemplate) {
        this.template = restTemplate;
    }

    /**
     * Home page of all Ratings
     *
     * @param model the model
     * @return HTML page with all Ratings
     */
    @GetMapping("/list")
    public String home(final Model model) {
        LOGGER.debug("GET request home page listAll Ratings OK");
        String findAllRatingsUrl = ConstantsURL.RATING_URL + "/findAll";
        ResponseEntity<List<RatingDTO>> responseEntity = template.exchange(
                findAllRatingsUrl, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<RatingDTO>>() {
                }
        );
        model.addAttribute("ratingList", responseEntity.getBody());
        return "rating/list";
    }

    /**
     * Add/Save Rating
     *
     * @param model the model
     * @return HTML save new BidList
     */
    @GetMapping("/add")
    public String addRatingForm(final Model model) {
        LOGGER.debug("GET request save/add Rating OK");
        RatingDTO rating = new RatingDTO();
        model.addAttribute(RATING_ATTRIBUTE, rating);
        return "rating/add";
    }

    /**
     * Validate Rating
     *
     * @param ratingDTO the Rating to save
     * @param result    the result
     * @param model     the model
     * @return HTML redirect to Rating after validate success
     */
    @PostMapping("/validate")
    public String validate(@Valid @ModelAttribute("rating") final RatingDTO ratingDTO,
                           final BindingResult result, final Model model) {
        LOGGER.debug("POST request validate Rating {} OK", ratingDTO.getId());
        String addRatingUrl = ConstantsURL.RATING_URL + "/add";
        if (!result.hasErrors()) {
            HttpEntity<RatingDTO> httpEntity = new HttpEntity<>(ratingDTO);
            template.exchange(addRatingUrl, HttpMethod.POST, httpEntity, String.class);
            LOGGER.info("Success validate Rating {}", ratingDTO.getId());
            return REDIRECTION_LINK;
        }
        LOGGER.error("Rating validate failed with ID {}", ratingDTO.getId());
        model.addAttribute(RATING_ATTRIBUTE, ratingDTO);
        return "rating/add";
    }

    /**
     * Update Rating
     *
     * @param id    id of Rating to update
     * @param model the model
     * @return HTML redirect to list of Rating after update
     */
    @GetMapping("/update")
    public String showUpdateForm(@RequestParam("id") final Integer id, final Model model) {
        LOGGER.debug("GET request update Rating {} OK", id);
        try {
            String findRatingById = ConstantsURL.RATING_URL + "/findById/" + id;
            ResponseEntity<RatingDTO> responseEntity = template.exchange(
                    findRatingById, HttpMethod.GET, null, RatingDTO.class);
            model.addAttribute(RATING_ATTRIBUTE, responseEntity.getBody());
            LOGGER.info("Update Rating success with ID : {}", id);
        } catch (HttpServerErrorException e) {
            LOGGER.error("Rating update failed with ID : {} ", id);
            return "404NotFound/404";
        }
        return "rating/update";
    }

    /**
     * Update Rating by ID
     *
     * @param id     the id of the Rating to update
     * @param rating the new Rating information
     * @param result the result
     * @param model  the model
     * @return HTML redirect of list Rating after success updated or form error
     */
    @PostMapping("/update/{id}")
    public String updateRating(@PathVariable("id") final Integer id,
                               @Valid @ModelAttribute("rating") final RatingDTO rating,
                               final BindingResult result, final Model model) {
        LOGGER.debug("POST request validate Rating by ID {} OK", id);
        String updateRatingUrl = ConstantsURL.RATING_URL + "/update/" + id;
        if (!result.hasErrors()) {
            HttpEntity<RatingDTO> httpEntity = new HttpEntity<>(rating);
            template.exchange(updateRatingUrl, HttpMethod.PUT, httpEntity, String.class);
            LOGGER.info("Rating update success {}", id);
            return REDIRECTION_LINK;
        }
        LOGGER.error("Rating update by ID failed : {}", id);
        model.addAttribute(RATING_ATTRIBUTE, rating);
        return "rating/update";
    }

    /**
     * Delete Rating
     *
     * @param id the id of the Rating to delete
     * @return HTML redirect to list of Rating or form error
     */
    @GetMapping("/delete")
    public String deleteRating(@RequestParam("id") final Integer id) {
        LOGGER.debug("GET request delete Rating {} OK", id);
        String deleteRating = ConstantsURL.RATING_URL + "/delete/" + id;
        try {
            template.exchange(deleteRating, HttpMethod.DELETE, null, String.class);
            LOGGER.info("Delete Rating success {}", id);
        } catch (HttpServerErrorException e) {
            LOGGER.error("Rating delete failed with ID : {}", id);
            return "404NotFound/404";
        }
        return REDIRECTION_LINK;
    }
}
