package com.nnk.poseidon.services;

import com.nnk.poseidon.domain.RuleName;
import com.nnk.poseidon.dto.RuleNameDTO;

import java.util.List;
import java.util.Optional;

public interface RuleNameService {

    Optional<RuleNameDTO> findRuleNameById(Integer id);
    List<RuleNameDTO> findAllRuleNames();
    RuleName saveRuleName(RuleName ruleName);
    RuleName updateRuleName(Integer id, RuleName ruleName);
    void deleteRuleName(Integer id);
    
}
