package com.nnk.poseidon.services;

import com.nnk.poseidon.domain.BidList;
import com.nnk.poseidon.dto.BidListDTO;

import java.util.List;

public interface BidListService {

    BidList save(BidList bidList);
    BidList updateBidList(Integer id, BidList bidList);
    BidListDTO findBidListById(Integer id);
    void deleteById(Integer id);
    List<BidListDTO> findAll();
}
