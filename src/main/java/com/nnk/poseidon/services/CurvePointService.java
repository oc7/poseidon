package com.nnk.poseidon.services;

import com.nnk.poseidon.domain.CurvePoint;
import com.nnk.poseidon.dto.CurvePointDTO;

import java.util.List;
import java.util.Optional;


public interface CurvePointService {

    Optional<CurvePointDTO> findCurvePointById(Integer id);
    List<CurvePointDTO> findAllCurvePoints();
    CurvePoint saveCurvePoint(CurvePoint curvePoint);
    CurvePoint updateCurvePoint(Integer id, CurvePoint curvePoint);
    void deleteCurvePointById(Integer id);
}
