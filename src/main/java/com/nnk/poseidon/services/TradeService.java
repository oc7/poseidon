package com.nnk.poseidon.services;

import com.nnk.poseidon.domain.Trade;
import com.nnk.poseidon.dto.TradeDTO;

import java.util.List;
import java.util.Optional;

public interface TradeService {

    Optional<TradeDTO> findTradeById(Integer id);
    List<TradeDTO> findAllTrades();
    Trade saveTrade(Trade trade);
    Trade updateTrade(Integer id, Trade trade);
    void deleteTrade(Integer id);
}
