package com.nnk.poseidon.services;

import com.nnk.poseidon.domain.Rating;
import com.nnk.poseidon.dto.RatingDTO;

import java.util.List;
import java.util.Optional;

public interface RatingService {

    Optional<RatingDTO> findRatingById(Integer id);
    List<RatingDTO> findAllRatings();
    Rating saveRating(Rating rating);
    Rating updateRating(Integer id, Rating rating);
    void deleteRating(Integer id);

}
