package com.nnk.poseidon.services;

import com.nnk.poseidon.converters.RuleNameConverter;
import com.nnk.poseidon.domain.RuleName;
import com.nnk.poseidon.dto.RuleNameDTO;
import com.nnk.poseidon.repositories.RuleNameRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class RuleNameServiceImpl implements RuleNameService {

    private static final Logger LOGGER = LogManager.getLogger(RuleNameServiceImpl.class);
    private final RuleNameRepository repository;
    private final RuleNameConverter converter;

    public RuleNameServiceImpl(final RuleNameRepository ruleNameRepository,
                               final RuleNameConverter ruleNameConverter) {
        this.repository = ruleNameRepository;
        this.converter = ruleNameConverter;
    }

    /**
     * Find RuleName by id
     *
     * @param id the RuleName id
     * @return the RuleName if found
     */
    @Override
    public Optional<RuleNameDTO> findRuleNameById(final Integer id) {
        Optional<RuleName> findById = repository.findById(id);
        if (findById.isPresent()) {
            LOGGER.info("RuleName {} loaded success", id);
            return Optional.ofNullable(converter
                    .ruleNameEntityToRuleNameDTOConverter(findById.get()));
        } else {
            LOGGER.error("Failed to load RuleName {}."
                    + " No resource found", id);
            throw new NoSuchElementException(
                    String.format("No resource found for id %s", id));
        }
    }

    /**
     * FindAll RuleName
     *
     * @return a list of all the RuleNames
     */
    @Override
    public List<RuleNameDTO> findAllRuleNames() {
        List<RuleName> result = repository.findAll();
        return converter.ruleNameEntityListToRuleNameDTOListConverter(result);
    }

    /**
     * Save RuleName
     *
     * @param ruleName the RuleName to save
     * @return a call to the repo layer
     */
    @Override
    public RuleName saveRuleName(final RuleName ruleName) {
        return repository.save(ruleName);
    }

    /**
     * Update RuleName
     *
     * @param id       the id of the RuleName to update
     * @param ruleName the new RuleName information
     * @return a call to the repo layer
     */
    @Override
    public RuleName updateRuleName(final Integer id, final RuleName ruleName) {
        Optional<RuleName> findRuleNameById = repository.findById(id);
        if (findRuleNameById.isPresent()) {
            LOGGER.info("RuleName {} updated success", id);
            ruleName.setId(findRuleNameById.get().getId());
            return repository.save(ruleName);
        } else {
            LOGGER.error("Failed to load RuleName {}. No resources found", id);
            throw new NoSuchElementException(String.format("Failed to update RuleName. No RuleName found with id %s", id));
        }
    }

    /**
     * Delete RuleName
     *
     * @param id the id of the RuleName to delete
     */
    @Override
    public void deleteRuleName(final Integer id) {
        Optional<RuleName> findRuleNameById = repository.findById(id);
        if (findRuleNameById.isPresent()) {
            repository.deleteById(id);
            LOGGER.info("RuleName {} delete success", id);
        } else {
            LOGGER.error("Failed to delete RuleName {}." + " No resource found for the provided id", id);
            throw new NoSuchElementException(String.format("Failed to Delete RuleName. No RuleName found with id %s", id));
        }
    }
}
