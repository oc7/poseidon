package com.nnk.poseidon.services;

import com.nnk.poseidon.domain.User;
import com.nnk.poseidon.dto.UserDTO;
import com.nnk.poseidon.exceptions.ResourceAlreadyExistsException;

import java.util.List;
import java.util.Optional;

public interface UserService {

    Optional<UserDTO> findById(Integer id);
    List<UserDTO> findAllUsers();
    User saveNewUser(User user) throws ResourceAlreadyExistsException;
    User updateUser(Integer id, User user);
    void deleteUser(Integer id);

}
